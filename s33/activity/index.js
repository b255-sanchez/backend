console.log("Hello world")


fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then(data => {
  	let titles = data.map(item => item.title);
    console.log(titles);
})



fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(json));


fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-type': 'application/json',
	},
	body: JSON.stringify({
		title: 'Created to do list item',
		body: 'Hello world',
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',

	headers: {
		'Content-type': 'application/json',
	},
	body: JSON.stringify({
		id: 1,
		dateCompleted:"Pending",
		description:"To update the my to do list with different data structure",
		status:"Pending",
		title: 'Updated to do list item',
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',

	headers: {
		'Content-type': 'application/json',
	},
	body: JSON.stringify({
		
		completed:false,
		dateCompleted:"07/09/21",
		id: 1,
		status:"Complete",
		title: 'delectus aut autem',
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1' ,{
	method:'DELETE'
});