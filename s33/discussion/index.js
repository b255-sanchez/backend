console.log("Hello world")

// [Section] Getting all posts
// The fetch API allows you to asynchronously request for a resorce (data)
// A "promise" is an object that reprepresents the eventual (or failure) or an asynchronous function and its resulting value

console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

// Retrieves all posts post following the rest API
// By using the then method we can now check for the status of the promise
fetch('https://jsonplaceholder.typicode.com/posts')
// the 'fetch' method will return a response object and returns another promise which will be eventually resolve or rejected
.then(response => console.log(response.status));

fetch('https://jsonplaceholder.typicode.com/posts')
// Use the "json" method from the response object to convert the data retrieved into JSON format to be used in our application
.then((response) => response.json())
// Print the converted JSON value from the "fetch" request
// Using multiple "then" method creates a "promise chain"
.then((json) => console.log(json));

// The "async" and "await" keywords is another approach that fcan be used to achieve asynchronous code
// Used in functions to indicate which portions of code should be waited for
// Creates an asynchronous function
async function fetchData(){

	// waits for the "fetch" method to complete then stores the value in the result variable
	let result = await fetch('https://jsonplaceholder.typicode.com/posts');
	// Result returned by fetch returns a promise
	console.log(result);
	// the returned reponse is an object
	console.log(typeof result);
	// we cannot access the content of response by directly accessing its body property
	console.log(result.body);

	// converts the data from the "response" ojbect as JSON
	let json = await result.json();
	// print out the content of the "Response" object
	console.log(json);
}

fetchData();

// [Section] getting a specific post

fetch('https://jsonplaceholder.typicode.com/posts/1')
.then((response) => response.json())
.then((json) => console.log(json));

// [SECTION] Creating a post
fetch('https://jsonplaceholder.typicode.com/posts', {
	// Sets the method of the request object to POST following the REST API
	// Default method is get
	method: 'POST',

	// Sets the header data of the request object to be sent to the backedn
	// Specified that the content will be in a JSON structure
	headers: {
		'Content-type': 'application/json',
	},
	// Sets the content/body data of the "request" object to be sent to backend
	// JSON.stringify converts the object data into a stringified JSON
	body: JSON.stringify({
		title: 'New Post',
		body: 'Hello world',
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// [Section] Updating a post using PUT method
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',

	headers: {
		'Content-type': 'application/json',
	},
	body: JSON.stringify({
		id: 1,
		title: 'Updated Post',
		body: 'Hello Again',
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// [Section] Updating a post using a PATCH
// Updates a specific post following the REST API
// The difference between PUT and PATCH is the number of properties being changed 
// Patch is used to update the whole object
// Put is used to update a single/several properties

fetch('https://jsonplaceholder.typicode.com/posts/1' , {

	method: 'PUT',
	headers: {
		'Content-type': 'application/json',
	},
	body: JSON.stringify({
		title: 'Corrected post',
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// [Section] Delete a post
// Deleting a specific post following the REST API
fetch('https://jsonplaceholder.typicode.com/posts/1' ,{
	method:'DELETE'
});

// [Section] Filtering posts
// The data can be filtered by sending the userID along with the URL
// Information sent via the url can be done by adding the question mark symbol (?)

fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
.then((response) => response.json())
.then((json) => console.log(json));

// Retrieving comments for specific post following the REST API


fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then((response) => response.json())
.then((json) => console.log(json));