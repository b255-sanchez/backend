console.log("Hello World");

// [Section] JSON objects
/*
	-JSON stands for Javascript Object Notation
	-JSON is aslo used in other programming languages hence the name Javascript Object Notation
	- Core Javascript has built in JSON Object that contains methods for parsing JSON objects and coverting string into Javascript Objects
	-Serialization is the process of converting data into a series of bytes for easier transmission/transfer of information
	-A byte is a unit of data that is eight binaray digits(1 and 0) that is used to represent a character(letters, numbers or typographic symbols)
	-Bytes are information that a computer processes to perform different tasks.
*/

// JSON Objects
/*{
	"city": "Quezon City",
	"province": "Metro Manila",
	"country": "Philippines",

}*/

// JSON Arrays
/*"cities"[
	{"city": "Quezon City", "province": "Metro Manila", "country": "Philippines"},
	{"city": "Manila City", "province": "Metro Manila", "country": "Philippines"},
	{"city": "Makati City", "province": "Metro Manila", "country": "Philippines"}
 
]*/

// [SECTION] Converting data into stringified JSON
/*
	-Stringified JSON is a Javascript object converted into string to be used in other functions of Javascript application
	-They are commonly used in HTTP requests where information is required to be sent and received in a stringfied JSON format
	-Requests are an important part of programming where an application communicates data with a backend application to perform different tasks such as getting/creating data in a database
	-A frontend application is an application that is used to interpret with users to perform different visual tasks and display information while backend applications are commonly used for all the business logic and data processing
	-A database normally stores information/data that can be used in most applications
	-Commonly stored data in databases are user information, transaction recoreds and product information
	-Node/Express JS are two type of technologies that are used for creating backend application with processes requests from frontend applications
*/

let batchesArr = [{batchName: 'Batch X'}, {batchName: 'Batch Y'}];
console.log('Result from stringify method: ');
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({
	name: 'John',
	age: 31,
	address: {
		city: 'Manila',
		country: 'Philippines'
	}
});

console.log(data);

// [SECTION] using stringify method with Variables
/*
	-When information is stored in a variable and is not hard coded into an object that is being stringified, we can supply the value with a varaible
	-The "property" name and "value" would have th esame name which can be confusing for beginners
	-This is done on purpose for code readability meaning when and information is stored in a variable and when the object created to be strinified is created, we supply the variable name instead of hardcoded value
	-This is commonly used when the information to be stored and sent to a backend application will come from frontend applciation
*/

let firstName = prompt('What is your first name?');
let lastName = prompt('What is your last name?');
let age = prompt('What is your age?');
let address = {
	city: prompt('Which city do you live in?'),
	country: prompt('Which country does your address belong to?')
};

let otherData = JSON.stringify({
	firstName : firstName,
	lastName : lastName,
	age : age,
	address : address
})

console.log(otherData);

// [SECTION] Converting stringified JSON into JavaScript objects
/*
	-Objects are common data types used in application because of the complex data structure that can be created out them
	-Information is commonly sent to applications in stringified JSON and then converted back into objects 
	-This happens both for sending information to a backend application

*/

let batchesJSON = '[{"batchName": "Batch X"}, {"batchName": "Batch Y"}]';

console.log("Result from parse method");
console.log(JSON.parse(batchesJSON));