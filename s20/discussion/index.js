console.log("Hello World");

// [Section] While loop

/*
	- A while loop takes in an expression/condition
	- Expressions are any unit of code that can be evaluatd to a value
	- If the condition evaluates to true, the statements inside the code block will be executed
	-A statement is a command the programmer gives to the computer
	-A loop will iterate a certain number of times until an expression/conditon is met

*/

let count = 5;
// While the value of count is not equal to 0

while (count !==0){
	// The current value of count is printed out
	console.log("While " + count);
	// Decreases the value of count by 1 after every iteration to stop the loop when it reaches 0
	// Make sure that expression/conditons in loops have their corresponding increment/decrement operators to stop the loop.
	count--;
}

// [Section] Do While loop

/*
	- A do-while loop works a lot like the while loop. But unlike the while loops, do-while loops guarantee that the code will bhe executed at least once.

*/

/*let number = Number(prompt("Give me a number: "))

do {
	console.log("Do while " + number);
	number +=1;
} while (number<10)
*/

// [Section] For loops

/*
	- A for loop is more flexible than while and do while loops.
	1. The initialization value that will track the progression of the loop.
	2. The expression/condition that will be evaluated which will determine whether the loop wil run one more time.
	3. The finalExpression indicates how to advance the loop.
	

*/

/*for(let count = 0; count <=20; count++){
	console.log(count);
}*/

let myString = 'alex';
// Characters in strings may be counted using the .length property
console.log(myString.length);

// Acessing elements of a string
console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);

for (let x = 0; x < myString.length; x++){
	console.log(myString[x]);
}

// Mini Activity
// Write a for loop the will print from 1 to 10
// It must be inside a function

/*function countNum(){

for (let x = 0; x <= 10; x++){
	console.log(x);
	}
}
countNum();
*/

console.log("Start of vowel loop")
let myName= "ALeX"
for(let i =0; i < myName.length; i++){
	// console.log(myName[i].toLowerCase())

	if (
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u"
	){
		console.log(3);
	} else {
		console.log(myName[i]);
	}
}

// [Section] Continue and Break statements
/*
	- The continue statement allows the code to go to the next iteration of the loop without finishing the execution of all statements
	- The break statement is sued to terminate the current loop once a match has been found
*/

for(let count = 0; count <=20; count++){
	if (count % 2 === 0){
		// Tells the code to continue to the next iteration of the loop
		continue;
	}

	console.log("Continue and break: " + count);

	if (count >10){
		// Tells the code to terminate/stop the loop even if the expression/condition of the loop defines that it should execute
		// number values after 10 will not be printed
		break;
	}
}

let name ="alexandro"

for(let i = 0; i < name.length; i++){
	console.log(name[i])

	if(name[i].toLowerCase() === 'a'){
		console.log("Continue to the next iteration");
		continue;
	}

	if(name[i] == 'd'){
		break;
	}
}