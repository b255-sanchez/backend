// console.log("Hello World");

//Objective 1
// Create a function called printNumbers that will loop over a number provided as an argument.
	//In the function, add a console log to display the number provided.
	//In the function, create a loop that will use the number provided by the user and count down to 0
		//In the loop, create an if-else statement:

			// If the counter number value provided is less than or equal to 50, terminate the loop and exactly show the following message in the console:
				//"The current value is at " + count + ". Terminating the loop."

			// If the counter number value is divisible by 10, skip printing the number and show the following message in the console:
				//"The number is divisible by 10. Skipping the number."

			// If the counter number  value is divisible by 5, print/console log the counter number.


let number = Number(prompt("Give me a number: "))
console.log("The number you provided is "+ number);

function printNumbers(){

	for(let count = number; count > 50; count-=5){
		if (number % 10 === 0){

			console.log("The number is divisible by 10. Skipping the number.");
			console.log(count);
			continue;
			
		}


		if (count > 50){

			break;
		}
	}
}

printNumbers();


//Objective 2
let string = 'supercalifragilisticexpialidocious';
console.log(string);
let filteredString = '';
for(let x = 0; x < string.length; x++){
	// console.log(myName[i].toLowerCase())
	let consonants = string[x];
	if (
		string[x].toLowerCase() == "a" ||
		string[x].toLowerCase() == "e" ||
		string[x].toLowerCase() == "i" ||
		string[x].toLowerCase() == "o" ||
		string[x].toLowerCase() == "u"
	){
		continue;
	} else {
		filteredString += consonants;

	}
}
console.log(filteredString);


//Add code here





//Do not modify
//For exporting to test.js
try {
    module.exports = {
       printNumbers, filteredString
    }
} catch(err) {

}