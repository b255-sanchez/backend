console.log("Hello World");

/*
	
//Note: strictly follow the variable names and function names from the instructions.



*/
// Addition
function addNum(num1, num2){
	console.log('Displayed sum of '+ num1 + ' and ' + num2);
	console.log(num1 + num2);
}
addNum(5, 15);

// Subtraction
function subNum(num1, num2){
	console.log('Displayed difference of '+ num1 + ' and ' + num2);
	console.log(num1 - num2);
}
subNum(20, 5);

// Multiplication
function multiplyNum(num1, num2){
	let product = num1 * num2;
	console.log('Displayed product of '+ num1 + ' and ' + num2);
	console.log(product);
}
multiplyNum(50, 10);

// Division
function divideNum(num1, num2){
	let quotient = num1 / num2;
	console.log('Displayed quotient of '+ num1 + ' and ' + num2);
	console.log(quotient);
}
divideNum(50, 10);

// Area of a circle
function getCircleArea(radius){
	let circleArea = (Math.PI * (radius ** 2));
	console.log('The result of getting the area of a circle with '+ radius + ' radius: ');
	console.log(circleArea);
}
getCircleArea(15);


// Average of four numbers
function getAverage(num1, num2, num3, num4){
	let averageVar = ((num1 + num2 + num3 + num4) / 4);
	console.log('The average of '+ num1 + ', ' + num2 + ', ' + num3 + ', ' + num4 + ':');
	console.log(averageVar);
}
getAverage(20, 40, 60, 80);

// check if passed
function checkIfPassed(score, total){
	let percentage = ((score * 100) / total);
	let isPassingScore = percentage >= 75;
	console.log('Is '+ score + '/' + total + ' a passing score? ');
	console.log(isPassingScore);
}
checkIfPassed(38, 50);





//Do not modify
//For exporting to test.js
try {
	module.exports = {
		addNum,subNum,multiplyNum,divideNum,getCircleArea,getAverage,checkIfPassed
	}
} catch (err) {

}