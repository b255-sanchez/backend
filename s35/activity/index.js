const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 4000;

// [SECTION] MongoDB connection
mongoose.connect("mongodb+srv://jed-255:admin123@zuitt-bootcamp.hg9hryb.mongodb.net/?retryWrites=true&w=majority", 
  {
    useNewUrlParser : true,
    useUnifiedTopology: true
  }
);


let db = mongoose.connection
db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database"))


const userSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  }
});



const User = mongoose.model("user", userSchema);


app.use(express.json());
// Allows your app to read data from forms
app.use(express.urlencoded({extended:true}));

app.post('/signup', async (req, res) => {
  const { username, password } = req.body;

 
  if (!username || !password) {
    return res.send({ message: 'Both username and password must be provided' });
  }
  try {
    // Check if the user already exists in the database
    const user = await User.findOne({ username });

    if (user) {
      return res.send('Duplicate username found');
    } else {
      // Create a new user
      const newUser = new User({ username, password });

      // Save the new user to the database
      await newUser.save();
      return res.send('New user registered');
    }
  } catch (err) {
    console.error(err);
    return res.send('Internal server error' );
  }
});

app.get("/signup", (req, res) => {

  User.find({}).then((result, err) => {

    if(err) {
      return console.log(err)
    } else {
      return res.status(200).json({
        data:result
      })
    }
  })
})


if(require.main === module){
  app.listen(port, () => console.log(`Server running at port ${port}`));

}

module.exports = app;
