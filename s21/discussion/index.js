console.log("Hello World");

// An Array in programmin is simply a list of data. Let's write the example earlier.

let studentNumberA = '2020-1923';
let studentNumberB = '2020-1924';
let studentNumberC = '2020-1925';
let studentNumberD = '2020-1926';
let studentNumberE = '2020-1927';

let studentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927'];

// [Section] Arrays

/*
	
	-Arrays are used to store multiple related values in a single variable
	-They are declared using square brackets[] also known as "Array Literals"
	-Commonly used to store numerous amounts of data to manipulate in order to perform a number of tasks.

*/

// Common examples for arrays
let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

// Possible use of an array but is not recommended
let mixedArr = [12, 'Asus', null, undefined, {}];

console.log(grades);
console.log(computerBrands);
console.log(mixedArr);


// Alternative way to write arrays
let myTasks = [
'drink html',
'eat javascript',
'inhale css',
'bake sass'
];

// Creating an array with values from variables
let city1 = 'Tokyo';
let city2 = 'Manila';
let city3 = 'Jakarta';

let cities = [city1, city2, city3];
console.log(myTasks);
console.log(cities);

// [Section] length property

// The .length property allows us to get and set the total number of items in an array

console.log(myTasks.length);
console.log(cities.length);

let blankArr = [];
console.log(blankArr.length);

// length property can also be used with strings. some array methods and properties can be also be used with strings.

let fullName = 'Jamie Noble'
console.log(fullName.length);

// length property can also set the total number of items in an array, meaning we can actually delete the last name

myTasks.length = myTasks.length-1;
console.log(myTasks.length);
console.log(myTasks);

// to delete a specific item in an array we can demploy array methods(which will be shown in the next session)
// Another example of using decrement
cities.length--;
console.log(cities);


fullName.length = fullName.length-1;
console.log(fullName.length);
fullName.length--;
console.log(fullName);

// If you can shorten the array by setting the length property, you can also lengthen it by adding a number into the length property

let theBeatles = ["John", "Paul", "Ringo", "George"];
theBeatles.length++
console.log(theBeatles);

// [Section] Reading from arrays
/*
	-Accessing array elements is one of the more common tasks that we can do with an array
	-This can be doone through the use of array indexes
	-Each element in an array is associated with it's own index/number
	-In JavaScript, the first element is associated with the number 0 and increasing this number 1 for every proceeding element
	-The reason an array statrs with 0 is due to how the language is designed
	-Array indexes actually refere to an address/location in the devices memory and how the information is stored
	-Example array location in memory
		Array[0] = 0X7ffe947bad0
		Array[0] = 0X7ffe947bad4
		Array[0] = 0X7ffe947bad8

*/
console.log(grades[0]);
console.log(computerBrands[0]);
// Accessomg an array element that does not exist will return "undefined"
console.log(grades[20]);

let lakersLegends = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"];
console.log(lakersLegends[1]);
console.log(lakersLegends[3]);

// You can save/store array items in another variable
let currentLaker = lakersLegends[2];
console.log(currentLaker);

// You can also reassign array values using the item's idices
console.log('Array before reassignment');
console.log(lakersLegends);
lakersLegends[2] = "Pau Gasol";
console.log('Array after reassignment');
console.log(lakersLegends);

blankArr[0] = "hello"
console.log(blankArr);

// Accessing the last element of an array
// Since the first element of an array starts with 0, subtracting 1 to the length of an array will offset the value by one allowing us to get the last index

let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];
let lastElementIndex = bullsLegends.length-1;

console.log(bullsLegends[lastElementIndex]);

// You can also add it directly
console.log(bullsLegends[bullsLegends.length-1]);

let newArr = [];
console.log(newArr[0]);

newArr[0] = "Cloud Strife";
console.log(newArr);

newArr[1] = "Tifa Lockhart";
console.log(newArr);


// You can also add items at the end of the array.
// newArr[newArr.length-1] = "Aerith Gainsborough";
newArr[newArr.length] = "Barret Wallace";
console.log(newArr);

// Looping over an array
// You can use a for loop to iterate over all items in an array
// Set the counter as the index and set a condition that as long as current index iterated is less than the length of an array

for(let index = 0; index<newArr.length; index++){
	console.log(newArr[index]);
}

let numArr = [5, 12, 30, 46, 40];
for (let index = 0; index < numArr.length; index++){
	if(numArr[index] % 5 ===0){
		console.log(numArr[index] + " is divisible by 5")
	} else{
		console.log(numArr[index] + " is not divisible by 5");
	}
}

// [Section] Multidimensional Array
/*
	-Multidimensional arrays are useful for storing complex data structures
	-A practical application of this is to help visualize/ create real world objects
	-Though useful in a number of cases, creating complex array structures is not always recommended.

*/

let chessBoard = [
	['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
	['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
	['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
	['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
	['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
	['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
	['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
	['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']

]

console.log(chessBoard);

// acessing elements of a multidimensional array 
console.log(chessBoard[1][4]);

console.log("Pawn moves to " + chessBoard[1][5]);