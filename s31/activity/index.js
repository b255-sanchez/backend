let http = require ("http");
http.createServer(function(request, response){
		response.writeHead(200, {"Content-Type": "text/plain"})

	response.end("hello world")


}).listen(3000)

console.log('Server is running at localhost:3000');

// 1. What directive is used by Node.js in loading the modules it needs?

// Require directive



// 2. What Node.js module contains a method for server creation?

// http module

// 3. What is the method of the http object responsible for creating a server using Node.js?

// http.createServer()

// 4. What method of the response object allows us to set status codes and content types?
	
// res.writeHead

// 5. Where will console.log() output its contents when run in Node.js?

// console

// 6. What property of the request object contains the address's endpoint?

// port