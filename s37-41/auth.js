const jwt = require("jsonwebtoken");
// User defined tring data that will be used to create our JSON web tokens
// Used in algorithm for encrypting our data which makes it difficult to decode the information without the defined secret keyword
const secret = "CourseBookingAPI";

// [Section] JSON web tokens
/*
	-JSON web token or JWT is a way of securely passing information from the server to the frontend or to other parts of the server
	-Information is kept secure through the use of the secret code
	-Only the system knows the secret code that can decode the enrypted information
*/

// Token Creation
module.exports.createAccessToken = (user) => {
	// The data will be received from the registration form
	// When the user logs in, a token will be created with the user's information
	const data = {
		id : user._id,
		email : user.email,
		isAdmin : user.isAdmin
	};

	// Generate a JSON web token using the jwt's sign method
	// Generates the token using the form data and the secret code with no additonal options provided
	return jwt.sign(data, secret, {});
}

// Token Verification
module.exports.verify = (req, res, next) => {
	// The token retrieved from the request header
	// This can be provided in postman under
		// Authorization > Bearer token
	let token = req.headers.authorization;

	// Token recieved and is not undefined
	if (typeof token !== "undefined"){
		console.log(token);

		// The "Slice" method takes only the token from the information sent via the request header
		// The token sent is a type of "bearer" token which when reacieved contains the word "Bearer" as a prefix to the string
		// This removes the "Bearer " prefix and obtains only the token for verification
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			if(err){
				// If JWT is not valid
				return res.send({auth : "failed"})
				// If JWT is valid
			} else {
				// Allows the application to proceed with the next middle function/callback function in the route
				// The verify method will be used as middleware in the route to verify the token vefore proceeding to the function that invokes the controller function
				next();
			}
		})
		// Token does not exist
	} else {
		return res.send({auth : "failed"})
	};
};

// Token decrpytion
module.exports.decode = (token) => {
	// token received and is not undefined
	if(typeof token !== "undefined"){
		// retrieves only the token and removes the bearer prefix
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data) => {

			if(err){
				return null;
			} else {
				// The "decode" method is used to obtain the information from the the JWT
				// The "{complete:true}" option allows us to return the additional information from the JWT token
				// Returns an object with access to the "payload" property whcih contains user information stored when the token was generated
				// The payload contains information provided in the "createAccessToken" method defined above(e.g. id, email and isAdmin)
 				return jwt.decode(token, {complete:true}.payload);
			}
		})
		// Token does not exist
	} else {
		return null;
	}
}