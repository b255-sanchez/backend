// Use the "require" directive to load Node.js modules
// A "module" is a software component or part of a program that contains one or more routines
// the "HTTP module" lets Node.js transfer data using the hyper text transfer protocol
// HTTP protocol that allows the fetching of resources such as HTML documents
// Clients(browser) and servers (nodejs/express js applications)communicate by exchanging individual messages
// the messages sent by the client, usually a web browser are called requests
// the messages sent by the server as an answer called responses

let http = require("http");


// Using this modules's createServer() method, we can create an HTTP server that listens to requests on a specified port and gives responses
// the http module has createServer() method that accepts a function as an argument and allows for a creation of a server
// The arguments posted in the function are requests and response objects (data type) that contains methods that allows us to receieve requests from the client and send responses back

http.createServer(function(request, response){

	// The http method of the incoming request can be be accessed via the "method"property of the "request" parameter
	// The method "Get" means that we will be retrieveing or reading information

	if(request.url == "/items" && request.method == "GET"){

		// requests the "/items" path and "GETS" information
		response.writeHead(200, {'Content-Type': 'text/plain'});
		// ends the reponse process
		response.end('Data retrieved from the database');
	}

	// The method "POST" means that we will be adding or creating information
	// In this example, we will just e sending a text reponse for now
	if(request.url == "/items" && request.method =="POST"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Data sent from the database');
	}
}).listen(4000);
console.log('Server is running at localhost:4000')