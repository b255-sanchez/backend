const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name : {
		type: String,
		
		required : [true, "Course is required"]
	},
	description : {
		type : String,
		required : [true, "Description is required"]
	},
	price : {
		type : String,
		required : [true, "Price is required"]
	},
	isActive : {
		type: Boolean,
		default: true
	},
	createdOn : {
		type : Date,
		default : new Date()
	},
	userOrders : [
		{
			userId : {
				type : String,
				required : [true, "UserId is required"]
			},

			status : {
				type : String,
				default : "Pending"
			}
		}
	] 
})

module.exports = mongoose.model("Product", productSchema);
