const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth");



router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});


router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});


router.get("/details", (req, res) => {


	userController.getProfile({userId : req.body.id}).then(resultFromController => res.send(resultFromController));

});



router.post("/order", auth.verify, (req,res) =>{
  let data = {
    userId: auth.decode(req.headers.authorization).id,
    productId: req.body.productId
  }

  userController.order(data).then(resultFromController => res.send (resultFromController));
})


module.exports = router;
