console.log("Hello World");

// Conditional Statements allows us to control the flow of our program. It allows us to run a statement/instruction if a condition is met or run another separate instruction if otherwise

// [SECTION] if, else if, else statement

let numA = -1;

if(numA < 0){
	console.log("Hello");
}

// The result of the expression added in the if's conditon must result to true, else, the statement inside if () will not run.

// You can also check the condition. The expression results to a boolean true because of the use of the less than operator.

console.log(numA<0);

numA = 0;

if(numA < 0){
	console.log("Hello again if numA is 0!");
}

// It will not run because the expression now results to false.
console.log(numA < 0);

let city = "New York";

if (city === "New York") {
	console.log("Welcome to New York City!");
}

// Else if clause
/*
	- Executes a statement if previous conditions are true
	- The "else if" clause is optional and can be added to capture additonal conditons to change the flow of a program
	
*/

let numH = 1;

if(numH < 0){
	console.log('Hello');
} else if (numH > 0){
	console.log('World');
}

// We were albe to run the else if() statement after we evaluated that the the if condition was failed

// If the if() conditon was passed and run, we will no longer evaluate the else if() and end the process there.

numA = 1;
if (numA > 0){
	console.log('Hello');
} else if (numH > 0){
	console.log('World');
}

// else if() statement no longer ran because the if statement

city = "Tokyo";

if(city === "New York"){
	console.log("Welcome to New York City");
} else if(city === "Tokyo"){
	console.log("Welcome to Tokyo, Japan!");
}

// Since we failed the conditon for the first if() we went to the else if() and checked and passed that condtion

// else statement
/*
	-Execute a statement if all other conditions are false
	- The "else" statement is optional and can be added to capture any other resuylt to change the flow of the program

*/

if(numA > 0){
	console.log('Hello');
} else if (numH === 0) {
	console.log('World')
} else {
	console.log('Error! NumA is not less than 0');
}

// Else statements should only be added if there is a preceding if condition. Else statements by itself will not work, however, if statements will work even if there is no else statement.

// If, else if and else statements with functions
/*
	-Most of the time we would like to use if, else if and else statements with functions to control the flow of our application
	-By including them inside function, we can decide when certain condtions will be checked instead of executing statements when the JavaScript loads.
	-The return statement can be utilized with conditonal statements in combination with functions to change values to be used for other features

*/

let message = 'No Message';
console.log(message);

function determineTyphoonIntensity(windSpeed) {
	if (windSpeed < 30){
		return 'Not a typhoon yet';
	}
	else if (windSpeed <=61){
		return 'Tropical depression detected';
	}
	else if (windSpeed >= 62 && windSpeed <= 88){
		return 'Tropical storm detected';
	}
	else if (windSpeed >= 89 || windSpeed <= 117){
		return 'Severe tropical storm detected';
	}
	else {
		return 'Typhoon Detected';
	}
}

message = determineTyphoonIntensity();
console.log(message);

// Mini- Activity
// Create a function with an if else statement inside
// The function should test for if a given number is even number or an odd number
// Console.log the output if it is add even number
// Console.log the output if it is add odd number

/*let num1
function determineNumber(number){
	if (num1 % 2 == 0);
	{
		console.log('Number is even')
	}
	else 
	{
		console.log('Number is odd')
	}
}

determineNumber(10);
determineNumber(19);

// Truth Examples */
/*
	-If the result of an expression in a condition results to a truthy value, the condition returns true and the corresponding statements are executed
	-Expressions are any unit of code that can be evaluated to be a value.

*/

if (true) {
	console.log('Truthy');
}
if (1) {
	console.log('Truthy');
}

if ([]) {
	console.log('Truthy');
}

// Falsy examples

if (0) {
	console.log('Falsy');
}

if (undefined) {
	console.log('Falsy');
}

// [Section] Conditional (ternary) Operator
/*

	-The conditional (Ternary) Operator takes in three operands
	1. condition
	2. expression to execute if the conditon is truthy
	3. expression to execute if the conditon is falsy
	- can be used as alternative to an "if else" statement
	-commonly used for single statement execution where there result consists of only one line of code

	Syntax:
	(Expression) ? ifTrue : ifFalse;

*/



// Single statement execution
let ternaryResult = (1<18) ? true : false;
console.log("Result of Ternary operator: " + ternaryResult);


// Multi statement execution

let name;

function isOfLegalAge(){
	name = 'John'
	return 'You are of the legal age limit'
}

/*
	-input receieved from the prompt function is returned as a string data type
	- the parseInt function coverts the input recieved into a number data type
*/

function isUnderAge(){
	name = 'Jane'
	return 'You are under the legal age limit'
}

let age = parseInt(prompt("What is your age?"));
console.log(age);
let legalAge = (age >18) ? isOfLegalAge() : isUnderAge();

console.log("Result of Ternary Operator in functions: " + legalAge + ', ' + name);


// [Section] Switch Statements

/*
	- The switch statement evaluates an expression and matches the expressions value to a case clause. The swutch will then execute the statements associated with that case, as well as statements in cases that follow the match

	- Switch cases are considered as "loops" meaning it will compared the expression with each of the case values until a match is found.
	- The "break" statement is used to terminate the current loop once a match has been found.

*/

let day = prompt ("What day of the week is it today") .toLowerCase();
console.log(day);

switch (day){
	case'monday' :
	console.log ("The color of the day is red");
	break;
	case'tuesday' :
	console.log ("The color of the day is orange");
	break;
	case'wednesday' :
	console.log ("The color of the day is yellow");
	break;
	case'thursday' :
	console.log ("The color of the day is green");
	break;
	case'friday' :
	console.log ("The color of the day is blue");
	break;
	case'saturday' :
	console.log ("The color of the day is indigo");
	break;
	case'sunday' :
	console.log ("The color of the day is violet");
	break;
	default:
	console.log ("Please input valid day");
	break;
}


// [Section] Try-catch-finally statement

/*

	-"try catch" statemets are commonly used for error handling
	-There are instantances when the application returns an error/warning that is not necessarilly an error in the context of our code
	-These errors are a result of an attempt of the programming langunage to  help developers in creating efficient code.
	-They are used to specify a response whenever an exception/error is recieved

*/

function showIntensityAlert(windSpeed){
	try {
		alertat(determineTyphoonIntensity(windSpeed));

	// error/err are commonly used variable names used by developers for storing errors
	} catch (error) {
		// The typeof operator is used to check the data type of value/expression and returns a stringvalue of what the data type is
		console.log(typeof error);

		// catch errors within the try statement
		// In this case the error is an unknown function 'alerat' which does not exist in JavaScript
		// "error.message" is used to access the infromation relating to an error object


		console.log(error.message);
	} finally {
		
		// continue execution of code regardless of success and failure of code execution in the 'try' block to handle/resolve errors
		alert('Intensity updates will show new alert');
	}
}

showIntensityAlert(56);