
// Created a database named hotel
// Inserted a single room using insertOne method
db.hotel.insertOne({
    "name": "Single",
    "accomodates": 2,
    "price" : 1000,
    "description" :"A simple room with all the basic necessities",
    "rooms_available" : 10,
    "isAvailable" : false
});

// Inserted a single room using insertMany method
db.hotel.insertMany([
    {
    "name": "Double",
    "accomodates": 3,
    "price" : 2000,
    "description" :"A room fit for a small family going on a vacation",
    "rooms_available" : 5,
    "isAvailable" : false
    },
    {
    "name": "Queen",
    "accomodates": 4,
    "price" : 4000,
    "description" :"A room with a queen sized bed perfect for a simple getaway",
    "rooms_available" : 15,
    "isAvailable" : false
    }
]);

// Using the find method
db.hotel.find({
    "name" : "Double"
});

// Using updateOne method
db.hotel.updateOne(
    {
        "name" : "Queen"
    },
    {
        $set:{
            "name": "Queen",
            "accomodates": 4,
            "price" : 4000,
            "description" :"A room with a queen sized bed perfect for a simple getaway",
            "rooms_available" : 0,
            "isAvailable" : false
        }
    }

);

// Delete many method
db.hotel.deleteMany({
    "rooms_available" : 0
});