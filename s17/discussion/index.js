console.log("hello world!");

// Functions
	// Functions in JavaScript are lines/blocks of codes that tell our device/ application to perform a certain task
	// Functions are mostly created to create complicated task to run several lines of code in sucession


// Function Declaration

	function printName(){
		console.log("My name is John");
	};

// Function Invocation
	
	printName();

// Function declarations vs expressions
	
	// Function declaration
	// A function can be created through function declaration by using the function keyword and adding a function name

	declaredFunction();

	function declaredFunction(){
		console.log("Hello World from declaredFunction()");
	};

	declaredFunction();

	// Function expression
	// A function can be also be stored in a variable. This is called function expression.

	// A function expression is an anonymous function assigned to the variableFunction

	// Anonymous function - a function without a name

	let variableFunction = function(){
		console.log("Hello Again!")
	};

	variableFunction();

	// We can also create a function expression of a name function
	// However, to invoke the function expression, we onvoke it by its variable name, not by its function name

	let funcExpression = function funcName(){
		console.log("Hello from the other side");
	}

	funcExpression();

	// You can reassign declared function and function expressions to new anonymous functions

	declaredFunction = function(){
		console.log("updated declaredFunction");
	}

	declaredFunction();

	funcExpression = function(){
		console.log("updated funcExpression");
	}

	funcExpression();

	// However we cannot re assign a function expression intialized with const

	const constantFunc = function(){
		console.log("Initialized with const!")
	}

	constantFunc();

	/*constantFunc = function(){
		console.log("cannot be reassigned!")
	}
	constantFunc();*/

// Function Scoping

/*	
	scope is the accesibility (visibility) of variables within our program
	
	JavaScript Variables has 3 types of scope:
	1. local/block scope
	2. global scope
	3. function scope

*/
	// Local block scope
	{
		let localVar= "Armando Perez";

	}

	// Global scope
	let gloabalVar = "Mr worldwide";

	console.log(gloabalVar);

	// Function scope

	function showNames(){
		var functionVar = "Joe";
		const functionConst = "John";
		let functionLet = "Jane";

		console.log(functionVar);
		console.log(functionConst);
		console.log(functionLet);
	}
	showNames();

	// Nested functions
		// You can create another inside a function. This is called a nested function.

		function myNewFunction(){
			let name = "Jane";

			function nestedFunction(){
				let nestedName ="John"
				console.log(name);
			}

			nestedFunction();
		}

		myNewFunction();

	// Function and Global Scoped variables
		// Global Scoped Variables
		let globalName = "Alexandro";

		function myNewFunction2(){
			let nameInside= "Renz";

			console.log(globalName);
		};

		myNewFunction2();

		// console.log(nameInside);

	// Using alert()

		// alert() allows us to show a small window at the top of our browser page to show information to our users. As opposed to console.log()

		alert("Hello world");

		function showSampleAlert(){
			alert("hello, user");
		};

		showSampleAlert();

		console.log("I will only log in the console when the alert is dismissed");

		// Using promt()
		// promt() allow us to show a small window at the browser to gather user input. Its much like alert() will have the page wait until it is dissmissed

		let samplePrompt = prompt ("Enter your name: ");
		// alert("Hello" + samplePrompt);
		console.log ("Hello, " + samplePrompt);




		function printWelcomeMessage() {
			let firstName = prompt("Enter your First Name");
			let lastName = prompt("Enter your Last Name");
			
			console.log("Hello, " + firstName + " " + lastName + "!");
			console.log("Welcome to my page!")

		};

		printWelcomeMessage();
		// Function Naming Conventions
		// Funtion names should be definitive of the task it will perform. It usually contains a verb.

		function getCourses(){
			let courses = ["Science 101", "Math 101", "English 101"];
			console.log(courses);
		}

		getCourses();

		// Avoid generic names to avoid confusion

		function get(){
			let name = "Jamie";
			console.log(name);
		}

		get();

		// Avoid pointless and inappropriate function names
		function foo() {
			console.log(25%5);
		}

		foo();

		// Name your funtions in small caps. Follow camelCase when naming variables and functions

		function displayCarInfo(){
			console.log("Brand: Toyota");
			console.log("Type: Sedan");
			console.log("Price: 1,500,500");
		}

		displayCarInfo();