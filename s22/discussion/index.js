console.log("Hello World")

// Array Methods

// JavaScript has built-in functions and methods for arrays. This allows us to manipulate and access array items.

// Mutator Methods
/*
	-Mutator Methods are functions that "mutate" pr change an array after they're created
	-These methods manipulate the orginal array performing various tasks sunch as adding and removing elements.
*/

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

/*
	-Adds an element in the end of an array and returns the array's length
*/

console.log('Current array:');
console.log(fruits);
let fruitsLength = fruits.push('Mango')
console.log(fruitsLength);
console.log("Mutated array from push method:")
console.log(fruits);

// Adding multiple elements to an array

fruits.push('Avocado', 'Guava');
console.log("Mutated array from push method:")
console.log(fruits);

// pop()
/*
	-Removes the last element in an array and returns the removed element
*/

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated array from pop method:")
console.log(fruits);

// unshift()
/*
	-adds an element at the beginning of array
*/
fruits.unshift('Lime', 'Banana');
console.log("Mutated array from unshift method:")
console.log(fruits);

// shift()
/*
	-removes an element at the beginning of array and returns the removed elemet
*/
let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift method:")
console.log(fruits);

// splice()
/*
	-simultaneosly removes elements from specified index number and adds elements
*/
fruits.splice(1, 2, 'Lime', 'Cherry');
console.log("Mutated array from splice method:")
console.log(fruits);

/*

	-Rearranges the array elements in aplhanumeric order
*/

fruits.sort();
console.log("Mutated array from sort method:")
console.log(fruits);

/*
	Important Note:
	-The "sort" method is used for more complicated sorting functions
*/

// reverse()
/*
	-Reverses the order for array elements
*/
fruits.reverse();
console.log("Mutated array from reverse method:")
console.log(fruits);

// Non-mutator methods
/*
	-Non-Mutator methods are functions that do not modify or change an array after they're created
	-These methods do not manipulate original array performing various tasks such as returning elements from an array and combining arrays.
*/

let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];

// indexof()
/*
	-Returns the index number of the first matching element found in an rray
	-if no match was found, the result will be -1
	-the search process will be done from the first element proceeding to the last element.

*/

let firstIndex = countries.indexOf('PH')
console.log('Reuslt of indexOf method: ' + firstIndex);
let invalidCountry = countries.indexOf('BR')
console.log('Reuslt of indexOf method: ' + invalidCountry);

// lastIndexOf()
/*
	-Returns the index number of the last matcing element found in an array
	-The search process will be done from the last element proceeding to the first elemet
*/

let lastIndex = countries.lastIndexOf('PH');
console.log('Reuslt of indexOf method: ' + lastIndex);

let lastIndexStart = countries.lastIndexOf('PH', 3);
console.log('Reuslt of indexOf method: ' + lastIndexStart);

// slice()
/*
	-Portions/Slice elements from an array AND returns a new array
*/

let slicedArrayA = countries.slice(2);
console.log('Reuslt of slice method: ');
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2, 4);
console.log('Reuslt of slice method: ');
console.log(slicedArrayB);

let slicedArrayC = countries.slice(-3);
console.log('Reuslt of slice method: ');
console.log(slicedArrayC);

// toString()
/*
	-Returns an array as a string separated by commas
*/
let stringArray = countries.toString();
console.log('Reuslt of toString method: ');
console.log(stringArray);

// concat()
/*
	-Combines two arrays and returns the combined result
*/

let taskArrayA = ['drink html', 'eat javascript'];
let taskArrayB = ['inhale html', 'breathe sass'];
let taskArrayC = ['get git', 'be node'];

let tasks = taskArrayA.concat(taskArrayB);
console.log('result from concat method: ');
console.log(tasks);

// combining multiple arrays
console.log('Result from concat method: ')
let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
console.log(allTasks);

// combining arrays with elements
let combinedTasks = taskArrayA.concat('smell express', 'throw react');
console.log('Result from concat method');
console.log(combinedTasks);

// join()
/*
	-returns an array as a string separeated by a specified
*/

let users = ['John', 'Jane', 'Joe', 'Robert'];

console.log(users.join());
console.log(users.join(''));
console.log(users.join(' - '));

// Iteration Methods
/*
	-Iteration methods are loops designed to perform repetitive tasks on arrays
	-Iteration methods loops over all items in an array
	-usefull for manipulating array data resulting in complex tasks
	-Array iteration methods normally work with a function supplied as an argument
	-how these function works is by performing tasks that are pre-define within an array's method
*/

// forEach()
/*
	-similar to a for loop that iterates on each array element
	-for each item in the array, the anonymous function pass in the forEach()method will be run
	-the anonymous function is able to receive the current item being iterated or loop over by assigning parameter
	-It's common practice to use the singular form of the array content for parameter names used in array loops.
*/


allTasks.forEach(function(task){
	console.log(task);
})

// using forEach with conditional statements
let filteredTasks = [];

allTasks.forEach(function(task){
	if(task.length > 10){
		filteredTasks.push(task);
	}
});

console.log('Result of filtered tasks: ');
console.log(filteredTasks);

// map()
/*
	-Iterates on each element and returns new array with diffrent values depending on th ereuslt of the function operations
	-This is useful for performing tasks where mutating/changing the elements are required
	-unlike the forEach method, the map method requires the use of "return" statement in order to create antoher array with the performed operation
*/

let numbers = [1, 2, 3, 4, 5]

let numberMap = numbers.map(function(numbers){
	return numbers * numbers;

})

console.log("Orginal Array: ")
console.log(numbers)// orginal array is unaffected by map()
console.log("Result of map method: ")
console.log(numberMap);//A new array is returnmed by map()

//map() vs forEach()

let numberForEach = numbers.forEach(function(numbers){
	return numbers = numbers
})
console.log(numberForEach);

// forEach(), loops over all items in ther array does map() but forEach() does not return a new array

// every()
/*
	-Checks if all elements in an array meet the given conditon
	-this is useful for validating data stored in arrays especially when dealing with large amounth of data
	-returns a true value if all elements meet the conditon and false if otherwise
*/

let allValid = numbers.every(function(numbers){
	return (numbers < 3);
})

console.log("Result of every method :");
console.log(allValid);

// some()
/*
	-Checks if at least one element in the array meets the given condition
	-Returns a true value if at least one element meets the conditon and false if otherwise

*/

let someValid = numbers.some(function(numbers){
	return (numbers < 2);
})

console.log("Result of some method :");
console.log(someValid);

// filter()
/*
	-Returns new array that contains elements which meets the given conditon
	-Returns an empty arry if no elements were found
	-Useful for filtering array elements with a given conditon shortens the syntax compared to using to other array iteration methods
	-matery of loops can help us work effectively by reducing the amount of code we use
*/

let filterValid = numbers.filter(function(numbers){
	return (numbers < 3);
})

console.log("Result of every method :");
console.log(filterValid);

// no elements found
let nothingFound = numbers.filter(function(numbers){
	return (numbers < 0);
})

console.log("Result of every method :");
console.log(nothingFound);

// Filtering using forEach
let filteredNumbers = []

numbers.forEach(function(numbers){
	if(numbers < 3){
		filteredNumbers.push(numbers)
	}
})

console.log("Result of filter method :");
console.log(filteredNumbers);

// includes()
/*
	-includes() method checks if the argument is passed can be found in the array
	-it returns a boolean which can be saved in a variable
		-returns true if the argument is found in the array
		-returns false if it is not
*/

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

let productsFound1 = products.includes("Mouse");
console.log(productsFound1);

let productsFound2 = products.includes("Headset");
console.log(productsFound2);


/*
	-Methods can be chained using them one after another
	-The result of the first method is used on the second method until all "chained" methods have resolved
*/

let filteredProducts = products.filter(function(products){
	return products.toLowerCase().includes('a');
})

console.log(filteredProducts);

// reduce()
/*
	-Evaluates elements from left to right and returns/reduces the array into a value
*/

let iteration = 0;
let reducedArray = numbers.reduce(function(x,y){
	// used to tract the current iteration count and accumulator/currentValue data
	console.warn('Current iteration ' + ++iteration);
	console.log('accumulator: ' + x);
	console.log('currentvalue: ' + y);

	return x +y;
})

console.log("Result of reduce method: " + reducedArray);

// reducing string arrays
let list = ['Hello', 'Again', 'World'];

let reducedJoin = list.reduce(function(x,y){
	return x + ' ' + y
});

console.log("Result of reduce method: " + reducedJoin);