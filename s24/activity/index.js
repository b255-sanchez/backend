// Exponent Operator
const getCube = (x) => x*x*x;
let x = 2
let total = getCube(x);
message = `The cube of ${x} is ${total}.`;
console.log(`${message}`);


// Template Literals
const address = ["258", "Washington Ave NW", "California", "90011"];
const [houseNumber, street, state, zipCode] = address;

message = `I live at ${houseNumber} ${street}, ${state} ${zipCode}`;
console.log(`${message}`)
// Array Destructuring


// Object Destructuring
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}
const {name, species, weight, measurement} = animal;

// console.log(name);
// console.log(species);
// console.log(weight);
// console.log(measurement);

console.log(`${name} was a ${species}. He weighed ${weight} with a measurement of ${measurement}.`)

// Arrow Functions
let numbers = [1, 2, 3, 4, 5];

numbers.forEach((num) => console.log(num));


// Javascript Classes
class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;

	}
}

const myDog = new Dog();
// console.log(myDog);

myDog.name = "Pryna";
myDog.age = "5";
myDog.breed = "Pomeranian";

console.log(myDog);

const myNewDog = new Dog("Frankie", "5", "Miniture Dachshund");

console.log(myNewDog);



//Do not modify
//For exporting to test.js
try {
	module.exports = {
		getCube,
		houseNumber,
		street,
		state,
		zipCode,
		name,
		species,
		weight,
		measurement,
		reduceNumber,
		Dog
	}	
} catch (err){

}