console.log("Hello World")
let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends:{
		hoenn: ['May', 'Max'],
		kanto: ['Brock', 'Misty']
	},

	  talk: function(){
	    return "Pikachu! I choose you";
	  }
	};
console.log(trainer);
console.log("Result of dot notation:");

let trainerName = [trainer];
console.log(trainerName[0].name);


console.log("Result of square bracket notation:");
let message = trainer.talk();
console.log(message);


// Create a constructor function called Pokemon for creating a pokemon

function Pokemon(name, level, health, attack){
	// Properties
	this.name = name;
	this.level = level;
	this.health = 2*level;
	this.attack = level;

	// Methods
  	this.tackle = function(target) {
    target.health -= this.attack;
    console.log(this.name + ' tackled ' + target.name);
    if (target.health <= 0) {
      console.log(target.name + "'s health is now reduced to " + target.health);
      this.faint(target);
     
    } else {
		
		console.log(target.name + "'s health is now reduced to " + target.health);
	};
  };
  
  	this.faint = function(target) {
    console.log(`${target.name} has fainted!`);
  };
}


	
let pikachu = new Pokemon("Pikachu", 12);
let geodude = new Pokemon("Geodude", 8);
let mewtwo = new Pokemon("Mewtwo", 100);
console.log(pikachu);
console.log(geodude);
console.log(mewtwo);

geodude.tackle(pikachu);
console.log(pikachu);
mewtwo.tackle(geodude);
console.log(geodude);

//Do not modify
//For exporting to test.js
try{
	module.exports = {
		trainer,
		Pokemon 
	}
} catch(err) {

}