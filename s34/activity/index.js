const express = require("express")
const app = express()
const port = 3000;

app.use(express.json());

app.use(express.urlencoded({extended:true}));

let users = [];
app.get("/home", (req, res) => {
	res.send("Welcome to the home page")
});


app.post("/users", (req, res) => {
	console.log(req.body);

	// if contents the "request.body" with the property "userName" and "password" is not empty
	if(req.body.userName !== '' && req.body.password !== ''){
		users.push(req.body);

		res.send(`User ${req.body.userName} successfully registered!`);
	} else {
		res.send("Please input both username and password")
	}
});




/*app.delete('/delete-user', (req, res) => {

  let message = '';


    // Loop through the array of users
    for (let i = 0; i < users.length; i++) {
      // If the current user's username matches the username in the request body
      if (users[i].userName === req.body.userName) {
        // Remove the user from the array using the splice method
        users.splice(i, 1);

        // Set the response message
        message = `User ${req.body.userName} has been deleted`;

        // Break out of the loop since we've found the user to delete
        break;
      }
    }


  

  // If the user was not found, set an error message
  if (message === '') {
    message = `User ${req.body.userName} not found`;
  }

  // Send the response message as a JSON object
  res.json({message});
  console.log(users)
});*/


app.delete("/delete-user", (req, res) =>{
	let message;

	// Creates a for loop that will loop through each element of the array
	for (let i = 0; i < users.length; i++){
		// If the username provided in the postman client and the username of the current object is the same
		if(req.body.userName == users[i].userName){
			// Changes password of the user found by the loop
			users.splice(i, 1);
			message = `User ${req.body.userName} has been deleted`;
			// breaks out of the loop once a user that matches the username provided is found
			break;
		} else{
			message = "User does not exist."
		}
	}
	res.send(message);
	console.log(users)
})



if (require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`))
}

module.exports = app;